# Template Tugas Akhir IPB 

Berikut adalah template tugas akhir Institut Pertanian Bogor berdasarkan Pedoman Penulisan Karya Ilmiah edisi ke-4 yang ditulis dalam format open document.

## Pedoman Penulisan Karya Ilmiah

Dalam menggunakan template ini dianjurkan untuk tetap membaca [buku PPKI edisi ke-4](https://simak.ipb.ac.id/Foto/Panduan/Dokumen_38.pdf) sebagai acuan.

## Libreoffice

Template tugas akhir ini ditulis dalam format open document sehingga dalam penggunaannya disarankan untuk menggunakan perangkat perkantoran Libreoffice. Perangkat perkantoran LibreOffice dapat diunduh [disini](https://www.libreoffice.org/download/download-libreoffice/).

## Zotero

Dalam manajemen pengutipan dan daftar pustaka dapat digunakan perangkat Zotero. Panduan penggunaan zotero dapat dilihat [disini](https://simak.ipb.ac.id/Foto/Panduan/Dokumen_44.pdf).
